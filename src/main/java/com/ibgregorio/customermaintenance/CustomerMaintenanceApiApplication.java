package com.ibgregorio.customermaintenance;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerMaintenanceApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CustomerMaintenanceApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
