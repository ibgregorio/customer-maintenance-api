package com.ibgregorio.customermaintenance.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Maintenance implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String reason;
	
	private LocalDateTime startPeriodMaintenance;
	
	private LocalDateTime endPeriodMaintenance;
	
	public Maintenance() {
	}

	public Maintenance(Long id, String reason, LocalDateTime startPeriodMaintenance,
			LocalDateTime endPeriodMaintenance) {
		super();
		this.id = id;
		this.reason = reason;
		this.startPeriodMaintenance = startPeriodMaintenance;
		this.endPeriodMaintenance = endPeriodMaintenance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDateTime getStartPeriodMaintenance() {
		return startPeriodMaintenance;
	}

	public void setStartPeriodMaintenance(LocalDateTime startPeriodMaintenance) {
		this.startPeriodMaintenance = startPeriodMaintenance;
	}

	public LocalDateTime getEndPeriodMaintenance() {
		return endPeriodMaintenance;
	}

	public void setEndPeriodMaintenance(LocalDateTime endPeriodMaintenance) {
		this.endPeriodMaintenance = endPeriodMaintenance;
	}
}
