package com.ibgregorio.customermaintenance.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibgregorio.customermaintenance.dto.MaintenanceDTO;
import com.ibgregorio.customermaintenance.entity.Maintenance;
import com.ibgregorio.customermaintenance.repository.MaintenanceRepository;
import com.ibgregorio.customermaintenance.service.exception.DataIntegrityException;
import com.ibgregorio.customermaintenance.service.exception.ObjectNotFoundException;

@Service
public class MaintenanceService {

	@Autowired
	private MaintenanceRepository maintenanceRepository;
	
	public Maintenance findMaintenanceById(Long idMaintenance) {
		Optional<Maintenance> maintenance = maintenanceRepository.findById(idMaintenance);
		
		return maintenance.orElseThrow(() -> new ObjectNotFoundException("Scheduled maintenance not found! Id: " + idMaintenance));
	}
	
	public List<Maintenance> listAllMaintenances() {
		return maintenanceRepository.findAll();
	}
	
	@Transactional
	public Maintenance saveMaintenance(Maintenance maintenance) {
		validateMaintenance(maintenance);
		
		return maintenanceRepository.save(maintenance);
	}
	
	@Transactional
	public Maintenance updateMaintenance(Maintenance maintenance) {
		Maintenance modifiedMaintenance = findMaintenanceById(maintenance.getId());
		validateMaintenance(maintenance);
		updateMaintenanceData(modifiedMaintenance, maintenance);
		
		return maintenanceRepository.save(modifiedMaintenance);
	}
	
	public void removeMaintenance(Long idMaintenance) {
		Maintenance selectedMaintenance = findMaintenanceById(idMaintenance);
		
		if (selectedMaintenance != null) {
			maintenanceRepository.deleteById(selectedMaintenance.getId());
		}
	}	
	
	public Maintenance buildEntityFromDTO(MaintenanceDTO maintenanceDTO) {
		return new Maintenance(
				maintenanceDTO.getId(), 
				maintenanceDTO.getReason(), 
				maintenanceDTO.getStartPeriodMaintenance(), 
				maintenanceDTO.getEndPeriodMaintenance());
	}
	
	private void updateMaintenanceData(Maintenance updatedMaintenance, Maintenance maintenance) {
		
		if (maintenance.getReason() != null) {
			updatedMaintenance.setReason(maintenance.getReason());
		}
		updatedMaintenance.setStartPeriodMaintenance(maintenance.getStartPeriodMaintenance());
		updatedMaintenance.setEndPeriodMaintenance(maintenance.getEndPeriodMaintenance());
	}
	
	private void validateMaintenance(Maintenance maintenance) {
		// Checks if informed Start Period is before the current date/time
		if (maintenance.getStartPeriodMaintenance().isBefore(LocalDateTime.now())) {
			throw new DataIntegrityException("Please inform a start period greater than current date/time");
		}
		
		// Checks if informed End Period is before the Start Period
		if (maintenance.getEndPeriodMaintenance().isBefore(maintenance.getStartPeriodMaintenance())) {
			throw new DataIntegrityException("End period must be greater than Start period");
		}
	}
}
