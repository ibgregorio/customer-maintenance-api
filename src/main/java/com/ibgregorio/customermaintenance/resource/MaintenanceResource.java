package com.ibgregorio.customermaintenance.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ibgregorio.customermaintenance.dto.MaintenanceDTO;
import com.ibgregorio.customermaintenance.entity.Maintenance;
import com.ibgregorio.customermaintenance.service.MaintenanceService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins = "${client.url}")
@RestController
@RequestMapping(value = "/v1/maintenances")
public class MaintenanceResource {

	@Autowired
	private MaintenanceService maintenanceService;
	
	@Operation(summary = "Find a Maintenance by Id")
	@ApiResponses(value = { 
			  @ApiResponse(responseCode = "200", description = "Shows the Maintenance found"),
			  @ApiResponse(responseCode = "404", description = "Maintenance not found", 
			  	content = @Content(mediaType = "application/json")) })
	@GetMapping(value = "/{idMaintenance}")
	public ResponseEntity<Maintenance> findMaintenanceById(@PathVariable Long idMaintenance) {
		Maintenance maintenance = maintenanceService.findMaintenanceById(idMaintenance);
		
		return ResponseEntity.ok().body(maintenance);
	}
	
	@Operation(summary = "Lists all scheduled Maintenances")
	@ApiResponses(value = { 
			  @ApiResponse(responseCode = "200", description = "Returns the list with all registered Maintenances") })
	@GetMapping
	public ResponseEntity<List<Maintenance>> listAllMaintenances() {
		List<Maintenance> listMaintenances = maintenanceService.listAllMaintenances();
		
		return ResponseEntity.ok().body(listMaintenances);
	}
	
	@Operation(summary = "Schedule a new Maintenance")
	@ApiResponses(value = { 
			  @ApiResponse(responseCode = "201", description = "Maintenance scheduled successfully"),
			  @ApiResponse(responseCode = "400", description = "Maintenance with invalid date/time period", 
			  	content = @Content(mediaType = "application/json")), 
			  @ApiResponse(responseCode = "422", description = "Validation error on Maintenance fields", 
			    content = @Content(mediaType = "application/json")) })
	@PostMapping
	public ResponseEntity<Void> saveMaintenance(@Valid @RequestBody MaintenanceDTO maintenanceDto) {
		Maintenance maintenanceEntity = maintenanceService.buildEntityFromDTO(maintenanceDto);
		
		maintenanceEntity = maintenanceService.saveMaintenance(maintenanceEntity);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(maintenanceEntity.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@Operation(summary = "Update an existing Maintenance")
	@ApiResponses(value = { 
			  @ApiResponse(responseCode = "200", description = "Maintenance updated successfully"),
			  @ApiResponse(responseCode = "400", description = "Maintenance with invalid date/time period", 
			  	content = @Content(mediaType = "application/json")), 
			  @ApiResponse(responseCode = "404", description = "Maintenance not found", 
			  	content = @Content(mediaType = "application/json")),
			  @ApiResponse(responseCode = "422", description = "Validation error on Maintenance fields", 
			    content = @Content(mediaType = "application/json")) })
	@PatchMapping(value = "/{idMaintenance}")
	public ResponseEntity<Void> updateMaintenance(@Valid @RequestBody MaintenanceDTO maintenanceDto, @PathVariable Long idMaintenance) {
		Maintenance maintenanceEntity = maintenanceService.buildEntityFromDTO(maintenanceDto);
		maintenanceEntity.setId(idMaintenance);
		maintenanceEntity = maintenanceService.updateMaintenance(maintenanceEntity);
		
		return ResponseEntity.ok().build();
	}
	
	@Operation(summary = "Delete an existing scheduled Maintenance")
	@ApiResponses(value = { 
			  @ApiResponse(responseCode = "204", description = "Maintenance deleted successfully"),
			  @ApiResponse(responseCode = "404", description = "Maintenance not found", 
			  	content = @Content(mediaType = "application/json")) })
	@DeleteMapping(value = "/{idMaintenance}")
	public ResponseEntity<Void> removeMaintenance(@PathVariable Long idMaintenance) {
		maintenanceService.removeMaintenance(idMaintenance);
		
		return ResponseEntity.noContent().build();
	}
}
