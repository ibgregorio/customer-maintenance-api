package com.ibgregorio.customermaintenance.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class MaintenanceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String reason;
	
	@NotNull(message = "Start Period is required")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private LocalDateTime startPeriodMaintenance;
	
	@NotNull(message = "End Period is required")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private LocalDateTime endPeriodMaintenance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDateTime getStartPeriodMaintenance() {
		return startPeriodMaintenance;
	}

	public void setStartPeriodMaintenance(LocalDateTime startPeriodMaintenance) {
		this.startPeriodMaintenance = startPeriodMaintenance;
	}

	public LocalDateTime getEndPeriodMaintenance() {
		return endPeriodMaintenance;
	}

	public void setEndPeriodMaintenance(LocalDateTime endPeriodMaintenance) {
		this.endPeriodMaintenance = endPeriodMaintenance;
	}
}
